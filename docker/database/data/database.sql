CREATE EXTENSION hstore;

-- site

CREATE TABLE IF NOT EXISTS site
(
  id SERIAL PRIMARY KEY NOT NULL,
  key UUID UNIQUE NOT NULL,
  is_active BOOLEAN NOT NULL DEFAULT true,
  is_deleted BOOLEAN NOT NULL DEFAULT false,
  created_at TIMESTAMP NOT NULL
);

INSERT INTO site (key, created_at) VALUES ('7522331f-cfac-45c7-b3ab-04194ecf0cde', NOW());
INSERT INTO site (key, created_at) VALUES ('47865486-8d7b-4555-b37a-835b30a5c9b3', NOW());
INSERT INTO site (key, created_at) VALUES ('37375348-b7c0-44a9-936a-fe744516a7da', NOW());
INSERT INTO site (key, created_at) VALUES ('7bbba435-5ab5-4363-bf42-6e0545e7ea8d', NOW());
INSERT INTO site (key, created_at) VALUES ('791f7fb3-0fe1-4ae9-a56d-31dddf1f4140', NOW());

CREATE INDEX index_site_id ON site USING HASH (id);
CREATE INDEX index_site_key ON site USING HASH (key);

-- site_data

CREATE TABLE IF NOT EXISTS site_data
(
  id SERIAL PRIMARY KEY NOT NULL,
  key UUID UNIQUE NOT NULL,
  site_id INTEGER NOT NULL REFERENCES site(id),
  extractor_site_data_id VARCHAR (255) NOT NULL,
  extractor_type VARCHAR (25) NOT NULL,
  data hstore,
  is_active BOOLEAN NOT NULL DEFAULT true,
  is_deleted BOOLEAN NOT NULL DEFAULT false,
  updated_at TIMESTAMP NOT NULL,
  created_at TIMESTAMP NOT NULL
);

INSERT INTO site_data (key, site_id, extractor_site_data_id, extractor_type, data, updated_at, created_at) VALUES ('7522331f-cfac-45c7-b3ab-04194ecf0cde', 1, 'ChIJr3spdvWYpBIRuQQb_jBT9kQ', 'google', '"paperback" => "243", "publisher" => "postgresqltutorial.com", "language" => "English", "ISBN-13" => "978-1449370000", "weight" => "11.2 ounces"', NOW(), NOW());
INSERT INTO site_data (key, site_id, extractor_site_data_id, extractor_type, data, updated_at, created_at) VALUES ('47865486-8d7b-4555-b37a-835b30a5c9b3', 2, 'jBT9kQ', 'tripadvisor', '"paperback" => "243", "publisher" => "postgresqltutorial.com", "language" => "English", "ISBN-13" => "978-1449370000", "weight" => "11.2 ounces"', NOW(), NOW());
INSERT INTO site_data (key, site_id, extractor_site_data_id, extractor_type, data, updated_at, created_at) VALUES ('37375348-b7c0-44a9-936a-fe744516a7da', 3, 'ChIJr3spdvWYpBIRuQQb', 'google', '"paperback" => "243", "publisher" => "postgresqltutorial.com", "language" => "English", "ISBN-13" => "978-1449370000", "weight" => "11.2 ounces"', NOW(), NOW());
INSERT INTO site_data (key, site_id, extractor_site_data_id, extractor_type, data, updated_at, created_at) VALUES ('7bbba435-5ab5-4363-bf42-6e0545e7ea8d', 4, 'ChIJr3spdvWYpBIR', 'google', '"paperback" => "243", "publisher" => "postgresqltutorial.com", "language" => "English", "ISBN-13" => "978-1449370000", "weight" => "11.2 ounces"', NOW(), NOW());
INSERT INTO site_data (key, site_id, extractor_site_data_id, extractor_type, data, updated_at, created_at) VALUES ('791f7fb3-0fe1-4ae9-a56d-31dddf1f4140', 5, 'uQQb_jBT9kQ', 'trustpilot', '"paperback" => "243", "publisher" => "postgresqltutorial.com", "language" => "English", "ISBN-13" => "978-1449370000", "weight" => "11.2 ounces"', NOW(), NOW());

CREATE INDEX index_site_data_id ON site_data USING HASH (id);
CREATE INDEX index_site_data_key ON site_data USING HASH (key);
