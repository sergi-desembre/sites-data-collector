<?php

declare(strict_types=1);

namespace DataCollector\Infrastructure\Bus\Event;

use DataCollector\Infrastructure\Bus\Event\Guard\DomainEventGuard;
use function Lambdish\Phunctional\get;
use Ramsey\Uuid\Uuid;

abstract class DomainEvent
{
    protected $eventId;
    protected $aggregateId;
    protected $data;
    protected $occurredOn;
    protected $metadata;

    public function __construct(
        string $aggregateId,
        array $data = [],
        string $eventId = null,
        int $occurredOn = null,
        array $metadata = []
    ) {
        $this->eventId = $eventId ?: Uuid::uuid4()->toString();
        $this->guardAggregateId($aggregateId);

        DomainEventGuard::guard($data, $this->rules(), get_called_class());

        $this->aggregateId = $aggregateId;
        $this->data        = $data;
        $this->occurredOn  = $occurredOn ?: $this->transformDate(new \DateTimeImmutable());
        $this->metadata    = $metadata;
    }

    abstract protected function rules(): array;

    abstract protected function generatedByDataCollector(): bool;

    public function eventId(): string
    {
        return $this->eventId;
    }

    public function aggregateId()
    {
        return $this->aggregateId;
    }

    public function data(): array
    {
        return $this->data;
    }

    public function metadata(string $key)
    {
        $metadata = get($key, $this->metadata);

        if (null === $metadata) {
            throw new \RuntimeException(sprintf('The key <%s> does not exist in metadata', $key));
        }

        return $metadata;
    }

    public function occurredOn(): string
    {
        return $this->occurredOn;
    }

    public function __call($method, $args)
    {
        $attributeName = $method;

        if (0 === strpos($method, 'is') && 1 === preg_match('/[A-Z]/', substr($method, 2, 1))) {
            $attributeName = lcfirst(substr($method, 2));
        }

        if (isset($this->data[$attributeName])) {
            return $this->data[$attributeName];
        }

        if (isset($this->rules()[$attributeName])) {
            return null;
        }

        throw new \RuntimeException(sprintf('The method "%s" does not exist.', $method));
    }

    protected function existAttribute($name): bool
    {
        return isset($this->data()[$name]);
    }

    abstract public static function eventName(): string;

    private function guardAggregateId($aggregateId)
    {
        if (!is_string($aggregateId) && !is_int($aggregateId)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'The Aggregate Id <%s> in <%s> is not valid, should be int or string.',
                    var_export($aggregateId, true),
                    get_class($this)
                )
            );
        }
    }

    private function transformDate(\DateTimeInterface $date): string
    {
        $timestamp             = $date->getTimestamp();
        $microseconds          = (int) $date->format('u');
        $millisecondsOnASecond = 1000;
        $microsecondsInASecond = 1000000;

        $numericDate = ((int) floor(
            (float) ($timestamp * $microsecondsInASecond + $microseconds) / $millisecondsOnASecond
        ));

        return (string) $numericDate;
    }
}
