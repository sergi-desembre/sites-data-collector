<?php

declare(strict_types=1);

namespace DataCollector\Infrastructure\Bus\Event;

use function Lambdish\Phunctional\apply;

final class DomainEventConsumer
{
    private $consumer;
    private $configuration;
    private $eventMapping;
    private $subscribersMapping;

    public function __construct(
        QueueConsumer $consumer,
        DomainEventSubscribersConfiguration $configuration,
        DomainEventMapping $eventMapping,
        SubscribersMapping $subscribersMapping
    ) {
        $this->consumer           = $consumer;
        $this->configuration      = $configuration;
        $this->eventMapping       = $eventMapping;
        $this->subscribersMapping = $subscribersMapping;
    }

    public function __invoke(string $consumer)
    {
        $config = $this->configuration->byName($consumer);

        return apply($this->consumer, [$config, $this->eventMapping->all(), $this->subscriber($config)]);
    }

    private function subscriber(DomainEventSubscriberConfiguration $domainEventSubscriberConfiguration)
    {
        return function (DomainEvent $event) use ($domainEventSubscriberConfiguration) {
            apply($this->subscribersMapping->byName($domainEventSubscriberConfiguration->name()), [$event]);
        };
    }
}
