<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Bus\Event;

final class DomainEventMapping
{
    private static $mapping = [];

    public function __construct(... $configDefaultData)
    {
        array_map(
            function (array $configData) {
                $this->add($configData['event_name'], $configData['event_class']);
            },
            $configDefaultData
        );
    }

    public function add(string $name, string $eventClass)
    {
        self::$mapping[$name] = $eventClass;
    }

    public function byName(string $name)
    {
        return self::$mapping[$name];
    }

    public function all(): array
    {
        return self::$mapping;
    }
}
