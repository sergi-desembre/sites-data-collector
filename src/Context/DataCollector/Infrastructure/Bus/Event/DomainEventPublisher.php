<?php

declare(strict_types=1);

namespace DataCollector\Infrastructure\Bus\Event;

interface DomainEventPublisher
{
    public function publish(array $domainEvents);

    public function raise(array $domainEvents);

    public function flush();
}
