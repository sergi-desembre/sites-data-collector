<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Bus\Event;

use function Lambdish\Phunctional\each;

final class DomainEventPublisherAsync implements DomainEventPublisher
{
    private $publisher;
    private $events = [];

    public function __construct(QueuePublisher $publisher)
    {
        $this->publisher = $publisher;
    }

    public function publish(array $domainEvents)
    {
        $this->raise($domainEvents);
        $this->flush();
    }

    public function raise(array $domainEvents)
    {
        $this->events = array_merge($this->events, array_values($domainEvents));
    }

    public function flush()
    {
        each($this->publisher, $this->pullEvents());
    }

    private function pullEvents()
    {
        $events       = $this->events;
        $this->events = [];

        return $events;
    }
}
