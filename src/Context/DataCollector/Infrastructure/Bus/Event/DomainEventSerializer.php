<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Bus\Event;

use function Lambdish\Phunctional\assoc;
use function Lambdish\Phunctional\reindex;

final class DomainEventSerializer
{
    public static function build(DomainEvent $event)
    {
        return json_encode(
            [
                'data' => [
                    'id'         => $event->eventId(),
                    'type'       => $event::eventName(),
                    'attributes' => reindex(
                        self::toSnake(),
                        assoc($event->data(), 'id', $event->aggregateId())
                    ),
                ],
                'meta' => [
                    'created_at' => (int) $event->occurredOn(),
                ],
            ]
        );
    }

    private static function toSnake()
    {
        return function ($unused, $key) {
            return ctype_lower($key) ? $key : strtolower(preg_replace('/([^A-Z\s])([A-Z])/', '$1_$2', $key));
        };
    }
}
