<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Bus\Event;

final class DomainEventSubscriberConfiguration
{
    private static $defaultConfig = [
        'processes'         => 1,
        'priority'          => 0,
        'queue'             => '',
        'events_to_process' => 200,
        'delay'             => 0,
        'enabled'           => true,
    ];
    private $config;

    public function __construct(array $config)
    {
        $this->config = array_merge(self::$defaultConfig, $config);
    }

    public function name(): string
    {
        return $this->config['name'];
    }

    public function queue(): string
    {
        return $this->config['queue'];
    }

    public function subscribedEvents(): array
    {
        return $this->config['subscribed_events'];
    }

    public function isSubscribedToEvent($eventName): bool
    {
        return in_array($eventName, $this->subscribedEvents());
    }

    public function hasQueue(): bool
    {
        return in_array('queue', $this->config);
    }
}
