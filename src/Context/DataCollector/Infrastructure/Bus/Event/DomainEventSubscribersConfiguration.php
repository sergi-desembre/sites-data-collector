<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Bus\Event;

use function Lambdish\Phunctional\filter;
use function Lambdish\Phunctional\map;
use function Lambdish\Phunctional\search;

final class DomainEventSubscribersConfiguration
{
    private static $config = [];

    public function __construct(... $configDefaultData)
    {
        array_map(function(array $configData) {
            $this->add($configData['name'], $configData);
            },
            $configDefaultData
        );
    }

    public function add(string $subscriber, array $config)
    {
        self::$config[$subscriber] = $config;
    }

    public function get(string $subscriber): DomainEventSubscriberConfiguration
    {
        return new DomainEventSubscriberConfiguration(self::$config[$subscriber]);
    }

    public function byName(string $name): DomainEventSubscriberConfiguration
    {
        return new DomainEventSubscriberConfiguration(search($this->byNameFinder($name), self::$config, []));
    }

    private function byNameFinder(string $name)
    {
        return function (array $config) use ($name) {
            return $name === $config['name'];
        };
    }

    /**
     * @return DomainEventSubscriberConfiguration[]
     */
    public function all(): array
    {
        return map($this->domainEventConfigCreator(), self::$config);
    }

    /**
     * @param string $name
     *
     * @return DomainEventSubscriberConfiguration[]
     */
    public function allWithEvent(string $name): array
    {
        return map($this->domainEventConfigCreator(), filter($this->containingEvent($name), self::$config));
    }

    private function domainEventConfigCreator(): \Closure
    {
        return function (array $configuration) {
            return new DomainEventSubscriberConfiguration($configuration);
        };
    }

    private function containingEvent(string $name): \Closure
    {
        return function (array $config) use ($name) {
            return in_array($name, $config['subscribed_events']);
        };
    }
}
