<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Bus\Event\Exception;

use RuntimeException;

final class DomainEventNotMappedException extends RuntimeException
{
    private $eventName;

    public function __construct($eventName)
    {
        parent::__construct(sprintf('The event <%s> has not any class to map', $eventName));

        $this->eventName = $eventName;
    }

    public function eventName()
    {
        return $this->eventName;
    }
}
