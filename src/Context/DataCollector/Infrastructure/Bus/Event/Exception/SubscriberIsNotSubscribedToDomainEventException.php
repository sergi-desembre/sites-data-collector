<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Bus\Event\Exception;

use RuntimeException;

final class SubscriberIsNotSubscribedToDomainEventException extends RuntimeException
{
    private $subscriberName;
    private $eventName;

    public function __construct(string $subscriberName, string $eventName)
    {
        parent::__construct(
            sprintf('The subscriber <%s> is not subscribed to the <%s> event', $subscriberName, $eventName)
        );

        $this->subscriberName = $subscriberName;
        $this->eventName      = $eventName;
    }

    public function subscriberName() : string
    {
        return $this->subscriberName;
    }

    public function eventName() : string
    {
        return $this->eventName;
    }
}
