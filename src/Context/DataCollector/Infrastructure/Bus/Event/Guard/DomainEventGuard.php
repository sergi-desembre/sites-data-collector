<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Bus\Event\Guard;

use App\Context\Shared\Types\Validator;
use DomainException;
use function Lambdish\Phunctional\all;
use function Lambdish\Phunctional\filter;

final class DomainEventGuard
{
    private static $error;

    public static function guard(array $data, array $rules, string $eventClass)
    {
        self::$error = '';

        if (!self::isValid($data, $rules)) {
            throw new DomainException(sprintf('Error constructing <%s>: %s', $eventClass, self::$error));
        }
    }

    private static function isValid(array $data, array $rules): bool
    {
        return self::hasAllParameters($data, $rules) && all(self::parameterIsValid($data), $rules);
    }

    private static function hasAllParameters(array $data, array $rules): bool
    {
        $missingParameters = filter(self::isNotOptional(), array_diff_key($rules, $data));

        if (!empty($missingParameters)) {
            self::addError(sprintf('The <%s> parameters are missing', implode(', ', array_keys($missingParameters))));

            return false;
        }

        return true;
    }

    private static function parameterIsValid(array $data)
    {
        return function (array $meta, string $attribute) use ($data) {
            if (!isset($data[$attribute])) {
                return true;
            }

            $value = $data[$attribute];

            if (null === $value && isset($meta[1]) && 'optional' === $meta[1]) {
                return true;
            }

            $isValid = Validator::isValid($value, self::extractType($meta));

            if (!$isValid) {
                $type = is_object($value) ? get_class($value) : gettype($value);

                self::addError(
                    sprintf('The parameter <%s> is invalid, <%s> expected but <%s> received', $attribute, $meta[0], $type)
                );
            }

            return $isValid;
        };
    }

    private static function extractType(array $meta)
    {
        return $meta[0];
    }

    private static function isNotOptional()
    {
        return function (array $data) {
            return !(isset($data[1]) and 'optional' === $data[1]);
        };
    }

    private static function addError(string $error)
    {
        self::$error = $error;
    }
}
