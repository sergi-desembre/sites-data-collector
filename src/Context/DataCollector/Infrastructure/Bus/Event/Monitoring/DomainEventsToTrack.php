<?php

declare(strict_types=1);

namespace DataCollector\Infrastructure\Bus\Event\Monitoring;

use DataCollector\Infrastructure\Bus\Event\DomainEvent;
use DataCollector\Module\Site\Application\Create\SiteScrappedDomainEvent;

final class DomainEventsToTrack
{
    public static function contains(DomainEvent $event)
    {
        return in_array(
            get_class($event),
            [
                SiteScrappedDomainEvent::class,
            ]
        );
    }
}
