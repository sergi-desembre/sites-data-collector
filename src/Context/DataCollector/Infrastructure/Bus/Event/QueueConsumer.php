<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Bus\Event;

interface QueueConsumer
{
    public function __invoke(
        DomainEventSubscriberConfiguration $domainEventSubscriberConfiguration,
        array $eventMapping,
        callable $subscriber
    ): int;
}
