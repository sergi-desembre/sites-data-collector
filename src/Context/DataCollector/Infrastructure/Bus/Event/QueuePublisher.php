<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Bus\Event;

interface QueuePublisher
{
    public function __invoke(DomainEvent $event, string $topic = null);
}
