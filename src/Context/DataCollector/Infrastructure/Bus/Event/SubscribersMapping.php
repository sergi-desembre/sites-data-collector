<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Bus\Event;

final class SubscribersMapping
{
    private static $mapping = [];

    public function __construct(...$configDefaultData)
    {
        array_map(function(array $configData) {
            $this->add($configData['subscriber_name'], $configData['subscriber_class']);
        }, $configDefaultData);
    }

    public function add(string $name, callable $subscriberClass)
    {
        self::$mapping[$name] = $subscriberClass;
    }

    public function byName(string $name) : callable
    {
        return self::$mapping[$name];
    }

    public function all(): array
    {
        return self::$mapping;
    }
}
