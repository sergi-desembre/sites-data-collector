<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Http\Guzzle;

use DataCollector\Infrastructure\Http\HttpClient;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class GuzzleClient implements HttpClient
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function callUrl(string $url, array $options = []): ResponseInterface
    {
        return $this->client->get($url, $options);
    }
}
