<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Http;

use Psr\Http\Message\ResponseInterface;

interface HttpClient
{
    public function callUrl(string $url, array $options = []): ResponseInterface;
}
