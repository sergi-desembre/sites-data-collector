<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Http;

use DataCollector\Infrastructure\Http\Guzzle\GuzzleClient;
use GuzzleHttp\Client;

final class HttpClientFactory
{
    public static function create(array $config = [])
    {
        return new GuzzleClient(new Client($config));
    }
}
