<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\InfluxDb;

use InfluxDB\Client;
use InfluxDB\Database;

final class InfluxDbDatabaseFactory
{
    public static function build(string $host, int $port, string $database): Database
    {
        $client = new Client($host, $port);

        return $client->selectDB($database);
    }
}
