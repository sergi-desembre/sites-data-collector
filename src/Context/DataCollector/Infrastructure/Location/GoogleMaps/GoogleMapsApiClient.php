<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Location\GoogleMaps;

use DataCollector\Infrastructure\Http\HttpClient;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

abstract class GoogleMapsApiClient
{
    private const RESPONSE_TIMEOUT        = 30;
    private const SUCCESS_RESPONSE_STATUS = 200;
    private const CLIENT_FORMAT_RESPONSE  = 'json';
    private $client;
    private $key;

    public function __construct(HttpClient $client, string $key)
    {
        $this->client = $client;
        $this->key    = $key;
    }

    protected function search(
        GoogleMapsApiSearchUrlAddress $googleMapsApiSearchUrlAddress,
        GoogleMapsApiSearchParameters $googleMapsApiSearchParameters
    ): array {
        $googleMapsApiSearchParameters->addParameter('key', $this->key);

        try {
            $response = $this->client->callUrl(
                sprintf(
                    '%s/%s?%s',
                    $googleMapsApiSearchUrlAddress->address(),
                    self::CLIENT_FORMAT_RESPONSE,
                    $googleMapsApiSearchParameters->queryParameters()
                ),
                ['timeout' => self::RESPONSE_TIMEOUT]
            );

            return $this->parseResponse($response);
        } catch (RequestException $exception) {
            return [];
        }
    }

    private function parseResponse(ResponseInterface $response): array
    {
        if (self::SUCCESS_RESPONSE_STATUS === $response->getStatusCode()) {
            $content = json_decode($response->getBody()->getContents(), true);

            return $content;
        } else {
            return [];
        }
    }
}
