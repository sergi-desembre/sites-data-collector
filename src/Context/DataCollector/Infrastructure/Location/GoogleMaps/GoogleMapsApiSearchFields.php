<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Location\GoogleMaps;

final class GoogleMapsApiSearchFields
{
    public const BASIC_FIELDS       = [
        'address_component',
        'adr_address',
        'alt_id',
        'formatted_address',
        'geometry',
        'icon',
        'id',
        'name',
        'permanently_closed',
        'photo',
        'place_id',
        'plus_code',
        'scope',
        'type',
        'url',
        'utc_offset',
        'vicinity',
    ];
    public const CONTACT_FIELDS     = [
        'formatted_phone_number',
        'international_phone_number',
        'opening_hours',
        'website',
    ];
    public const CONTACT_ATMOSPHERE = ['price_level', 'rating', 'review'];
    private $fields;

    private function __construct(array $fields)
    {
        foreach ($fields as $field) {
            $this->addField($field);
        }
    }

    public function plainFields(string $separator = ','): string
    {
        return implode($separator, $this->fields);
    }

    public function addField(string $field)
    {
        $this->fields[] = $field;
    }

    public static function buildAsBasic(): GoogleMapsApiSearchFields
    {
        return new self(self::BASIC_FIELDS);
    }

    public static function buildAsContact(): GoogleMapsApiSearchFields
    {
        return new self(self::CONTACT_FIELDS);
    }

    public static function buildAsAtmosphere(): GoogleMapsApiSearchFields
    {
        return new self(self::CONTACT_ATMOSPHERE);
    }
}
