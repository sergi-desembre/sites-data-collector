<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Location\GoogleMaps;

final class GoogleMapsApiSearchParameters
{
    private const     PARAMETER_NAME_PLACE_ID    = 'placeid';
    private const     PARAMETER_NAME_KEYWORD     = 'input';
    private const     PARAMETER_NAME_INPUT_TYPE  = 'inputtype';
    private const     PARAMETER_VALUE_INPUT_TYPE = 'textquery';
    private const     PARAMETER_NAME_FIELDS      = 'fields';
    private $parameters;

    private function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    public function addParameter(string $field, string $value)
    {
        $this->parameters[$field] = $value;
    }

    public function queryParameters(): string
    {
        return urldecode(http_build_query($this->parameters));
    }

    public static function buildAsSearch(string $keyword): GoogleMapsApiSearchParameters
    {
        return new self(
            [
                self::PARAMETER_NAME_KEYWORD    => $keyword,
                self::PARAMETER_NAME_INPUT_TYPE => self::PARAMETER_VALUE_INPUT_TYPE,
            ]
        );
    }

    public static function buildAsDetail(
        string $placeId,
        GoogleMapsApiSearchFields $googleMapsApiSearchFields = null
    ): GoogleMapsApiSearchParameters {
        $searchFields = $googleMapsApiSearchFields === null ? $googleMapsApiSearchFields :
            $googleMapsApiSearchFields->plainFields();

        return new self(
            [
                self::PARAMETER_NAME_PLACE_ID => $placeId,
                self::PARAMETER_NAME_FIELDS   => $searchFields,
            ]
        );
    }
}
