<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Location\GoogleMaps;

final class GoogleMapsApiSearchUrlAddress
{
    private const STATIC_URL                 = 'https://maps.googleapis.com/maps/api/place';
    private const PARAMETER_EXTENSION_SEARCH = 'findplacefromtext';
    private const PARAMETER_EXTENSION_DETAIL = 'details';
    private $paramExtensions;

    private function __construct(array $paramExtension)
    {
        $this->paramExtensions = $paramExtension;
    }

    public function address(): string
    {
        return sprintf(
            '%s/%s',
            self::STATIC_URL,
            implode('/', $this->paramExtensions)
        );
    }

    public static function buildAsSearch()
    {
        return new self([self::PARAMETER_EXTENSION_SEARCH]);
    }

    public static function buildAsDetail()
    {
        return new self([self::PARAMETER_EXTENSION_DETAIL]);
    }
}
