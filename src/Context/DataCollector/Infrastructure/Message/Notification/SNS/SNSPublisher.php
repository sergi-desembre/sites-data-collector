<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Message\Notification\SNS;

use Aws\Sns\SnsClient;
use DataCollector\Infrastructure\Bus\Event\DomainEventSerializer;
use DataCollector\Infrastructure\Bus\Event\QueuePublisher;
use Psr\Log\LoggerInterface;
use DataCollector\Infrastructure\Bus\Event\DomainEvent;

final class SNSPublisher implements QueuePublisher
{
    private $client;
    private $logger;
    private $topic;

    public function __construct(SnsClient $client, LoggerInterface $logger, $topic = null)
    {
        $this->client = $client;
        $this->topic  = $topic;
        $this->logger = $logger;
    }

    public function __invoke(DomainEvent $event, string $topic = null)
    {
        $topicArn = $this->topic ?: $topic;

        $this->logger->debug('Publishing domain event', ['name' => $event::eventName(), 'topic' => $topicArn]);

        $this->client->publish(
            [
                'TopicArn' => $topicArn,
                'Message'  => DomainEventSerializer::build($event),
            ]
        );
    }
}
