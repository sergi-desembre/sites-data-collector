<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Message\Queue\RabbitMQ;

use App\Context\Shared\Domain\Monitoring\Monitor;
use App\Context\Shared\Utils\JsonDeserialize;
use DataCollector\Infrastructure\Bus\Event\DomainEvent;
use DataCollector\Infrastructure\Bus\Event\DomainEventSubscriberConfiguration;
use DataCollector\Infrastructure\Bus\Event\Exception\SubscriberIsNotSubscribedToDomainEventException;
use DataCollector\Infrastructure\Bus\Event\QueueConsumer;
use DataCollector\Infrastructure\Message\Queue\SQS\DomainEventUnserializer;
use function Lambdish\Phunctional\apply;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

final class RabbitMQConsumer implements QueueConsumer
{
    private const CONSUMER_TAG = '';
    private const NO_LOCAL     = false;
    private const NO_ACK       = false;
    private const EXCLUSIVE    = false;
    private const NO_WAIT      = false;
    private const WAIT_TIMEOUT = 30;
    private $connection;
    private $logger;
    private $monitor;

    public function __construct(AMQPStreamConnection $connection, LoggerInterface $logger, Monitor $monitor)
    {
        $this->connection = $connection;
        $this->logger     = $logger;
        $this->monitor    = $monitor;
    }

    public function __invoke(
        DomainEventSubscriberConfiguration $domainEventSubscriberConfiguration,
        array $eventMapping,
        callable $subscriber
    ): int {
        $channel = $this->connection->channel();

        try {
            $channel->basic_consume(
                $domainEventSubscriberConfiguration->queue(),
                self::CONSUMER_TAG,
                self::NO_LOCAL,
                self::NO_ACK,
                self::EXCLUSIVE,
                self::NO_WAIT,
                function (AMQPMessage $message) use (
                    $subscriber,
                    $eventMapping,
                    $domainEventSubscriberConfiguration,
                    $channel
                ) {
                    $this->consumer(
                        $subscriber,
                        $this->extractEvent($message, $eventMapping),
                        $domainEventSubscriberConfiguration
                    );

                    $channel->basic_ack($message->delivery_info['delivery_tag']);
                }
            );

            $this->waitIfThereAreNoMessages($channel);

            $channel->close();
            $this->connection->close();
        } catch (AMQPTimeoutException $exception) {
            $this->logger->info($exception->getMessage());
        }

        return 1;
    }

    private function consumer(
        callable $consumer,
        DomainEvent $event,
        DomainEventSubscriberConfiguration $domainEventSubscriberConfiguration
    ) {
        $this->guardEventIsValidForSubscriber($event, $domainEventSubscriberConfiguration);

        apply($consumer, [$event]);
    }

    private function extractEvent(AMQPMessage $message, array $eventMapping): DomainEvent
    {
        $unserializer = new DomainEventUnserializer($eventMapping);

        return apply($unserializer, [JsonDeserialize::run($message->getBody())]);
    }

    private function waitIfThereAreNoMessages(AMQPChannel $channel)
    {
        while (count($channel->callbacks)) {
            $this->logger->alert('Waiting for incoming messages');
            $channel->wait(null, false, self::WAIT_TIMEOUT);
        }
    }

    private function guardEventIsValidForSubscriber(
        DomainEvent $event,
        DomainEventSubscriberConfiguration $domainEventSubscriberConfiguration
    ) {
        $name = $event::eventName();

        if (!$domainEventSubscriberConfiguration->isSubscribedToEvent($name)) {
            throw new SubscriberIsNotSubscribedToDomainEventException(
                $domainEventSubscriberConfiguration->name(), $name
            );
        }
    }
}
