<?php

declare(strict_types=1);

namespace DataCollector\Infrastructure\Message\Queue\RabbitMQ;

use App\Context\Shared\Domain\Monitoring\Monitor;
use DataCollector\Infrastructure\Bus\Event\DomainEvent;
use DataCollector\Infrastructure\Bus\Event\DomainEventSerializer;
use DataCollector\Infrastructure\Bus\Event\Monitoring\DomainEventsToTrack;
use DataCollector\Infrastructure\Bus\Event\QueuePublisher;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

final class RabbitMQPublisher implements QueuePublisher
{
    private const DEFAULT_CONSUMER_TYPE = 'fanout';

    private $connection;
    private $logger;
    private $monitor;

    public function __construct(AMQPStreamConnection $connection, LoggerInterface $logger, Monitor $monitor)
    {
        $this->connection = $connection;
        $this->logger     = $logger;
        $this->monitor    = $monitor;
    }

    public function __invoke(DomainEvent $event, string $topic = null)
    {
        $this->logger->debug('Published domain event', ['name' => $event::eventName(), 'topic' => $topic]);

        if (DomainEventsToTrack::contains($event)) {
            $this->monitor->onePoint(['domain-event' => $event::eventName()]);
        }

        $this->publishMessage($event);
    }

    private function publishMessage(DomainEvent $event)
    {
        $channel = $this->createChannel($event::eventName());

        $channel->basic_publish(
            new AMQPMessage(
                DomainEventSerializer::build($event),
                [
                    'type'       => $event::eventName(),
                    'timestamp'  => $event->occurredOn(),
                    'message_id' => $event->eventId(),
                ]
            ),
            $event::eventName()
        );
    }

    private function createChannel(string $channelName): AMQPChannel
    {
        $channel = $this->connection->channel();

        $channel->exchange_declare($channelName, self::DEFAULT_CONSUMER_TYPE, false, true, false);
        $channel->queue_declare($channelName, false, true, false, false);
        $channel->queue_bind($channelName, $channelName);

        return $channel;
    }
}
