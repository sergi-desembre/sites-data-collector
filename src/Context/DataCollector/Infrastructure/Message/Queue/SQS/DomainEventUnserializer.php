<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Message\Queue\SQS;

use DataCollector\Infrastructure\Bus\Event\Exception\DomainEventNotMappedException;
use function Lambdish\Phunctional\dissoc;
use function Lambdish\Phunctional\get;
use function Lambdish\Phunctional\get_in;
use function Lambdish\Phunctional\reindex;
use Ramsey\Uuid\Uuid;

final class DomainEventUnserializer
{
    private $eventMapping;

    public function __construct(array $eventMapping)
    {
        $this->eventMapping = $eventMapping;
    }

    public function __invoke($serializedEvent)
    {
        $eventName  = $serializedEvent['data']['type'];
        $eventId    = $serializedEvent['data']['id'];
        $eventClass = $this->findEventFor($eventName);
        $attributes = $serializedEvent['data']['attributes'];
        $occurredOn = get_in(['meta', 'created_at'], $serializedEvent);
        $metadata   = get('meta', $serializedEvent, []);

        return new $eventClass(
            get('id', $attributes, Uuid::uuid4()->toString()),
            reindex($this->toCamel(), dissoc($attributes, 'id')),
            $eventId,
            $occurredOn,
            $metadata
        );
    }

    private function findEventFor($eventName)
    {
        $this->guardExists($eventName);

        return $this->eventMapping[$eventName];
    }

    private function guardExists($eventName)
    {
        if (!isset($this->eventMapping[$eventName])) {
            throw new DomainEventNotMappedException($eventName);
        }
    }

    private function toCamel(): \Closure
    {
        return function ($unused, $key) {
            return lcfirst(str_replace('_', '', ucwords($key, '_')));
        };
    }
}
