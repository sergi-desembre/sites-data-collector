<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Message\Queue\SQS;

use Aws\Sqs\SqsClient;

final class SQSClientFactory
{
    public static function create()
    {
        return new SqsClient(
            [
                'credentials' => [
                    'key'    => 'AKIAJ2AHET5EVKH6GPDQ',
                    'secret' => '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',
                ],
                'region'      => 'eu-west-1',
                'version'     => 'latest',
            ]
        );
    }
}
