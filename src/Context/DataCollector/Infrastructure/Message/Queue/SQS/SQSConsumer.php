<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Message\Queue\SQS;

use App\Context\Shared\Exception\DeserializeErrorException;
use App\Context\Shared\Utils\JsonDeserialize;
use Aws\Sqs\SqsClient;
use DataCollector\Infrastructure\Bus\Event\DomainEvent;
use DataCollector\Infrastructure\Bus\Event\DomainEventSubscriberConfiguration;
use DataCollector\Infrastructure\Bus\Event\Exception\DomainEventNotMappedException;
use DataCollector\Infrastructure\Bus\Event\Exception\SubscriberIsNotSubscribedToDomainEventException;
use DataCollector\Infrastructure\Bus\Event\QueueConsumer;
use function Lambdish\Phunctional\apply;
use function Lambdish\Phunctional\each;
use Psr\Log\LoggerInterface;

final class SQSConsumer implements QueueConsumer
{
    private const MESSAGES_TO_CONSUME                        = 10;
    private const SECONDS_TO_WAIT_WHEN_THERE_ARE_NO_MESSAGES = 2;
    private const VISIBILITY_TIMEOUT                         = 30;
    private const WAIT_TIME_SECONDS                          = 20;
    private const EMPTY_MESSAGES_VALUE                       = 0;
    private $client;
    private $logger;

    public function __construct(SqsClient $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    public function __invoke(
        DomainEventSubscriberConfiguration $domainEventSubscriberConfiguration,
        array $eventMapping,
        callable $subscriber
    ): int {
        $messages           =
            $this->client->receiveMessage($this->clientParameters($domainEventSubscriberConfiguration->queue()));
        $unserializedEvents = $messages->get('Messages') ?: [];

        $this->waitIfThereAreNoMessages($unserializedEvents);

        $unserializer = new DomainEventUnserializer($eventMapping);

        each($this->consumer($subscriber, $unserializer, $domainEventSubscriberConfiguration), $unserializedEvents);

        return count($unserializedEvents);
    }

    private function consumer(
        callable $consumer,
        callable $unserializer,
        DomainEventSubscriberConfiguration $domainEventSubscriberConfiguration
    ) {
        return function (array $unserializedEvent) use ($consumer, $unserializer, $domainEventSubscriberConfiguration) {
            try {
                $unserializedBody = JsonDeserialize::run($unserializedEvent['Body']);
                $event            = apply($unserializer, [JsonDeserialize::run($unserializedBody['Message'])]);

                $this->guardEventIsValidForSubscriber($event, $domainEventSubscriberConfiguration);
                apply($consumer, [$event]);

                $this->ack($domainEventSubscriberConfiguration->queue(), $unserializedEvent['ReceiptHandle']);
            } catch (DomainEventNotMappedException $exception) {
                $this->logger->warning(
                    'Event not mapped. Maybe is an old just deleted event?. This event is going to be deleted.',
                    [
                        'event_name' => $exception->eventName(),
                        'event_body' => $unserializedEvent['Body'],
                    ]
                );

                $this->ack($domainEventSubscriberConfiguration->queue(), $unserializedEvent['ReceiptHandle']);
            } catch (SubscriberIsNotSubscribedToDomainEventException $exception) {
                $this->logger->warning(
                    'Event is not subscribed to subscriber. Maybe is an old just unsubscribed event?. This event is going to be deleted.',
                    [
                        'event_name'      => $exception->eventName(),
                        'subscriber_name' => $exception->subscriberName(),
                        'event_body'      => $unserializedEvent['Body'],
                    ]
                );

                $this->ack($domainEventSubscriberConfiguration->queue(), $unserializedEvent['ReceiptHandle']);
            } catch (DeserializeErrorException $exception) {
                $this->logger->warning(
                    'Error deserializing domain event. Re-queueing.',
                    [
                        'deserialize_error' => $exception->lastErrorMessage(),
                        'event_body'        => $unserializedEvent['Body'],
                    ]
                );

                sleep(1);

                throw new SQSDomainEventConsumptionError($exception);
            } catch (\Throwable $exception) {
                $this->logger->warning(
                    'Error processing domain event. Re-queueing.',
                    [
                        'event_body' => $unserializedEvent['Body'],
                    ]
                );

                sleep(1);

                throw new SQSDomainEventConsumptionError($exception);
            }
        };
    }

    private function ack($queue, $receiptHandle)
    {
        $this->client->deleteMessage(
            [
                'QueueUrl'      => $queue,
                'ReceiptHandle' => $receiptHandle,
            ]
        );
    }

    private function clientParameters($queue)
    {
        return [
            'QueueUrl'            => $queue,
            'VisibilityTimeout'   => self::VISIBILITY_TIMEOUT,
            'WaitTimeSeconds'     => self::WAIT_TIME_SECONDS,
            'MaxNumberOfMessages' => self::MESSAGES_TO_CONSUME,
        ];
    }

    private function waitIfThereAreNoMessages(array $messages)
    {
        if (self::EMPTY_MESSAGES_VALUE === count($messages)) {
            sleep(self::SECONDS_TO_WAIT_WHEN_THERE_ARE_NO_MESSAGES);
        }
    }

    private function guardEventIsValidForSubscriber(
        DomainEvent $event,
        DomainEventSubscriberConfiguration $domainEventSubscriberConfiguration
    ) {
        $name = $event::eventName();

        if (!$domainEventSubscriberConfiguration->isSubscribedToEvent($name)) {
            throw new SubscriberIsNotSubscribedToDomainEventException(
                $domainEventSubscriberConfiguration->name(), $name
            );
        }
    }
}
