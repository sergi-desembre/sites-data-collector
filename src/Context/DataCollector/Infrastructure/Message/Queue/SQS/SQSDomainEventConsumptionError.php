<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Message\Queue\SQS;

use App\Context\Shared\Exception\DomainErrorException;

final class SQSDomainEventConsumptionError extends DomainErrorException
{
    private $originalError;

    public function __construct(\Throwable $originalError)
    {
        $this->originalError = $originalError;

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'sqs_domain_event_consumption_error';
    }

    protected function errorMessage(): string
    {
        return 'Error consuming domain event';
    }
}
