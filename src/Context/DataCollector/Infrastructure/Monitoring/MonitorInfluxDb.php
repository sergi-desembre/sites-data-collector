<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Monitoring;

use App\Context\Shared\Domain\Monitoring\Monitor;
use InfluxDB\Database;
use InfluxDB\Point;

final class MonitorInfluxDb implements Monitor
{
    private const ONE_POINT_MEASUREMENT   = 'site_data_collector_counters';
    private const ONE_POINT_VALUE         = 1.00;
    private const ONE_POINT_FIELD_METRIC  = 'metric';
    private const ONE_POINT_FIELD_COUNTER = 'counter';
    private $influxDatabase;

    public function __construct(Database $influxDatabase)
    {
        $this->influxDatabase = $influxDatabase;
    }

    public function onePoint(array $tags)
    {
        try {
            $this->influxDatabase->writePoints(
                [
                    new Point(
                        self::ONE_POINT_MEASUREMENT,
                        self::ONE_POINT_VALUE,
                        array_merge([self::ONE_POINT_FIELD_METRIC => self::ONE_POINT_FIELD_COUNTER], $tags),
                        [],
                        (int) microtime(true)
                    ),
                ],
                Database::PRECISION_SECONDS
            );
        } catch (\Exception $unused) {
        }
    }
}
