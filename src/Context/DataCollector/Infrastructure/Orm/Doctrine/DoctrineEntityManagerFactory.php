<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Orm\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\SimplifiedYamlDriver;
use Doctrine\ORM\Tools\Setup;

final class DoctrineEntityManagerFactory
{
    private static $sharedPrefixes = [
        __DIR__ . '/../../../../Shared/Infrastructure/Persistence' => 'App\Context\Shared\Domain',
    ];

    public static function create(array $parameters, array $prefixes, bool $isDevMode)
    {
        $configuration = Setup::createConfiguration($isDevMode);

        $configuration->setMetadataDriverImpl(
            new SimplifiedYamlDriver(
                array_merge(self::$sharedPrefixes, $prefixes)
            )
        );

        return EntityManager::create($parameters, $configuration);
    }
}
