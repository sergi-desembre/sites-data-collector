<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Orm\Doctrine;

use App\Context\Shared\Types\AggregateRoot;
use App\Context\Shared\Types\AggregateRootCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

abstract class Repository
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function entityManager(): EntityManager
    {
        return $this->entityManager;
    }

    protected function persist(AggregateRoot $entity)
    {
        $this->entityManager()->persist($entity);
        $this->entityManager()->flush($entity);
    }

    protected function remove(AggregateRoot $entity)
    {
        $this->entityManager()->remove($entity);
        $this->entityManager()->flush($entity);
    }

    protected function deleteMultiple(AggregateRootCollection $aggregates)
    {
        array_map(function (AggregateRoot $entity){
            $this->entityManager()->remove($entity);
        }, $aggregates->items());

        $this->entityManager()->flush($aggregates->items());
    }

    protected function persistMultiple(AggregateRootCollection $aggregates)
    {
        array_map(function (AggregateRoot $entity){
            $this->entityManager()->persist($entity);
        }, $aggregates->items());

        $this->entityManager()->flush($aggregates->items());
    }

    protected function persister()
    {
        return function (AggregateRoot $entity) {
            $this->persist($entity);
        };
    }

    protected function remover()
    {
        return function (AggregateRoot $entity) {
            $this->remove($entity);
        };
    }

    protected function repository($entityClass): EntityRepository
    {
        return $this->entityManager->getRepository($entityClass);
    }

    protected function queryBuilder()
    {
        return $this->entityManager->createQueryBuilder();
    }
}
