<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\Orm\Doctrine;

use function Lambdish\Phunctional\apply;

final class SiteEntityManagerFactory
{
    private static $namespace = 'DataCollector\Module';
    private static $prefixes = [
        'Site\Domain' => 'Module/Site/Infrastructure/Persistence',
    ];

    public static function create(array $parameters, string $rootPath, bool $isDevMode)
    {
        return DoctrineEntityManagerFactory::create(
            $parameters,
            self::getNormalizedPrefixes($rootPath),
            $isDevMode
        );
    }

    private static function getNormalizedPrefixes($analyticsContextRootPath)
    {
        return apply(new PrefixesNormalizer(realpath($analyticsContextRootPath), self::$namespace), [self::$prefixes]);
    }
}
