--
-- Table structure for table `google_maps_site`
--

CREATE TABLE IF NOT EXISTS google_maps_site
(
  id INT unsigned AUTO_INCREMENT PRIMARY KEY NOT NULL,
  uuid BINARY(16) UNIQUE NOT NULL,
  google_id VARCHAR(50) UNIQUE NOT NULL,
  google_maps_site_data_version_id INT NOT NULL,
  hash CHAR(32) NOT NULL
  is_active BIT(1) NOT NULL DEFAULT 1,
  is_deleted BIT(1) NOT NULL DEFAULT 0,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  INDEX (id, uuid, google_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
