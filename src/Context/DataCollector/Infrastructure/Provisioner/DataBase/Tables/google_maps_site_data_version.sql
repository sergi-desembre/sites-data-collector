--
-- Table structure for table `google_maps_site_data_version`
--

CREATE TABLE IF NOT EXISTS google_maps_site_data_version
(
  id INT unsigned AUTO_INCREMENT PRIMARY KEY NOT NULL,
  google_maps_site_id INT NOT NULL,
  uuid BINARY(16) UNIQUE NOT NULL,
  name VARCHAR(255) NOT NULL,
  phone VARCHAR(30),
  address VARCHAR(255) NOT NULL,
  url TEXT NOT NULL,
  rating_value TINYINT(1) NOT NULL,
  rating_max_value TINYINT(1) NOT NULL,
  latitude DECIMAL(10, 8) NOT NULL,
  longitude DECIMAL(11, 8) NOT NULL,
  is_active BIT(1) NOT NULL DEFAULT 1,
  is_deleted BIT(1) NOT NULL DEFAULT 0,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  INDEX (id, google_maps_site_id, uuid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
