--
-- Table structure for table `google_maps_site_review`
--

CREATE TABLE IF NOT EXISTS google_maps_site_review
(
  id INT unsigned AUTO_INCREMENT PRIMARY KEY NOT NULL,
  google_maps_site_id INT NOT NULL,
  uuid BINARY(16) UNIQUE NOT NULL,
  author VARCHAR(70) NOT NULL,
  author_url TEXT NOT NULL,
  author_image VARCHAR(255) NOT NULL,
  language CHAR(2) NOT NULL,
  rating_value TINYINT(1) NOT NULL,
  rating_max_value TINYINT(1) NOT NULL,
  comment TEXT NOT NULL,
  creation_date TIMESTAMP NOT NULL,
  is_active BIT(1) NOT NULL DEFAULT 1,
  is_deleted BIT(1) NOT NULL DEFAULT 0,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),
  INDEX (id, google_maps_site_id, uuid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
