<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\UI\Console\Symfony;

use DataCollector\Infrastructure\Bus\Event\DomainEventConsumer;
use function Lambdish\Phunctional\apply;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CreateSite extends Command
{
    private const QUANTITY_EVENTS_PROCESS = 10;
    private const TOTAL_RETRIES           = 5;
    private const SUBSCRIBER_NAME         = 'data_collector_subscriber_site_scrapped';
    private $consumer;
    private $totalRetries = 0;

    public function __construct(DomainEventConsumer $domainEventConsumer)
    {
        parent::__construct(null);

        $this->consumer = $domainEventConsumer;
    }

    protected function configure()
    {
        parent::configure();

        $this
            ->setName('data-collector:create-site')
            ->setDescription('Consume domain events to create sites');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $remainingToProcess = self::QUANTITY_EVENTS_PROCESS;

        while ($remainingToProcess > 0 && $this->hasNotExceededTheRetriesLimit()) {
            $remainingToProcess =
                $this->eventsToProcess($remainingToProcess, apply($this->consumer, [self::SUBSCRIBER_NAME]));
        }
    }

    private function eventsToProcess(int $totalEventsToProcess, int $processedEvents): int
    {
        if (0 === $processedEvents) {
            $this->totalRetries++;
        }

        return $totalEventsToProcess - $processedEvents;
    }

    private function hasNotExceededTheRetriesLimit(): bool
    {
        return self::TOTAL_RETRIES > $this->totalRetries;
    }
}
