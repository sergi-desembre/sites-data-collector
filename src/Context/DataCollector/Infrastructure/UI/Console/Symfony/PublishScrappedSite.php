<?php

declare(strict_types = 1);

namespace DataCollector\Infrastructure\UI\Console\Symfony;

use DataCollector\Infrastructure\Bus\Event\DomainEventPublisher;
use DataCollector\Infrastructure\Uuid\UuidGenerator;
use DataCollector\Module\Site\Application\Create\SiteScrappedDomainEvent;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class PublishScrappedSite extends Command
{
    private $publisher;

    public function __construct(DomainEventPublisher $publisher)
    {
        parent::__construct(null);

        $this->publisher = $publisher;
    }

    protected function configure()
    {
        parent::configure();

        $this
            ->setName('data-collector:publish-scrapped-site')
            ->setDescription('Publish example scrapped site to queue')
            ->addArgument(
                'keyword',
                InputArgument::OPTIONAL,
                'Keyword text to search to create site',
                'Campo de futbol Camp Nou'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $siteScrappedDomainEvent = new SiteScrappedDomainEvent(
            UuidGenerator::generate(),
            [
                'keyword' => $input->getArgument('keyword'),
            ]
        );

        $this->publisher->publish([$siteScrappedDomainEvent]);

        print_r($siteScrappedDomainEvent);
        echo PHP_EOL;
    }
}
