<?php

declare(strict_types = 1);

namespace DataCollector\Module\Site\Application\Add;

final class AddSiteDataQuery
{
    private $keyword;

    public function __construct(string $keyword)
    {
        $this->keyword = $keyword;
    }

    public function keyword(): string
    {
        return $this->keyword;
    }
}
