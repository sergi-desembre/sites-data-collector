<?php

declare(strict_types = 1);

namespace DataCollector\Module\Site\Application\Add;

use function Lambdish\Phunctional\apply;
use function Lambdish\Phunctional\pipe;

final class AddSiteDataQueryHandler
{
    private $adder;

    public function __construct(SiteDataAdder $adder)
    {
        $this->adder = pipe(
            $adder,
            new SiteDataMarshaller()
        );
    }

    public function __invoke(AddSiteDataQuery $query)
    {
        return apply($this->adder, [$query->keyword()]);
    }
}
