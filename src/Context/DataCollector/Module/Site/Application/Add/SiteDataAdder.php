<?php

declare(strict_types = 1);

namespace DataCollector\Module\Site\Application\Add;

use App\Context\Shared\Types\AggregateRoot;
use DataCollector\Infrastructure\Bus\Event\DomainEvent;
use DataCollector\Infrastructure\Bus\Event\DomainEventPublisher;
use DataCollector\Module\Site\Domain\ExternalAskSiteRepository;
use DataCollector\Module\Site\Domain\GoogleMapsSite\GoogleMapsSite;
use DataCollector\Module\Site\Domain\GoogleMapsSite\GoogleMapsSiteCollection;
use DataCollector\Module\Site\Domain\GoogleMapsSite\GoogleMapsSiteCreatedDomainEvent;

final class SiteDataAdder extends AggregateRoot
{
    private $repository;
    private $publisher;

    public function __construct(ExternalAskSiteRepository $repository, DomainEventPublisher $publisher)
    {
        $this->repository = $repository;
        $this->publisher  = $publisher;
    }

    public function __invoke(string $keyword): GoogleMapsSiteCollection
    {
        $sites = $this->repository->searchByKeyword($keyword);

        $this->publishSites($sites);

        return $sites;
    }

    private function publishSites(GoogleMapsSiteCollection $sites)
    {
        $this->publisher->publish(
            array_map(
                function (GoogleMapsSite $site) {
                    return $this->createSiteDomainEvent($site);
                },
                $sites->items()
            )
        );
    }

    private function createSiteDomainEvent(GoogleMapsSite $site): DomainEvent
    {
        $currentDateTime = new \DateTime();

        return new GoogleMapsSiteCreatedDomainEvent(
            $site->formattedUuid(),
            [
                'externalId' => $site->googleId(),
                'name'       => $site->name(),
                'url'        => $site->url()->completeAddress(),
                'createdAt'  => $currentDateTime->getTimestamp(),
            ]
        );
    }
}
