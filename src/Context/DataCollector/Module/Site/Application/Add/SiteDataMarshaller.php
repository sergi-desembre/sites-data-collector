<?php

declare(strict_types = 1);

namespace DataCollector\Module\Site\Application\Add;

use DataCollector\Module\Site\Domain\GoogleMapsSite\GoogleMapsSite;
use DataCollector\Module\Site\Domain\GoogleMapsSite\GoogleMapsSiteCollection;

final class SiteDataMarshaller
{
    public function __invoke(GoogleMapsSiteCollection $sites)
    {
        return array_map(
            function (GoogleMapsSite $site) {
                return [
                    'external_id' => $site->googleId(),
                    'uuid'        => $site->formattedUuid(),
                    'name'        => $site->name(),
                    'address'     => $site->address(),
                    'longitude'   => $site->location()->longitude(),
                    'latitude'    => $site->location()->latitude(),
                    'url_address' => $site->url()->completeAddress(),
                    'phone'       => $site->phone(),
                    'hash'        => $site->hash(),
                ];
            },
            $sites->items()
        );
    }
}
