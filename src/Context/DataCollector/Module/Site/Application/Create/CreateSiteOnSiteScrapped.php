<?php

declare(strict_types = 1);

namespace DataCollector\Module\Site\Application\Create;

use DataCollector\Module\Site\Domain\SiteKeyword;
use function Lambdish\Phunctional\apply;

final class CreateSiteOnSiteScrapped
{
    private $creator;

    public function __construct(SiteCreator $creator)
    {
        $this->creator = $creator;
    }

    public function __invoke(SiteScrappedDomainEvent $event)
    {
        $siteKeyword = new SiteKeyword($event->keyword());

        apply($this->creator, [$siteKeyword->keyword()]);
    }
}
