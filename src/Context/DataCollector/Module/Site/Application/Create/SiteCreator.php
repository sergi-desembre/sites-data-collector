<?php

declare(strict_types = 1);

namespace DataCollector\Module\Site\Application\Create;

use App\Context\Shared\Domain\Site\SiteCreatedDomainEvent;
use App\Context\Shared\Types\AggregateRoot;
use DataCollector\Infrastructure\Bus\Event\DomainEvent;
use DataCollector\Infrastructure\Bus\Event\DomainEventPublisher;
use DataCollector\Module\Site\Domain\Site;
use DataCollector\Module\Site\Domain\SiteRepository;

final class SiteCreator extends AggregateRoot
{
    private $repository;
    private $publisher;

    public function __construct(SiteRepository $repository, DomainEventPublisher $publisher)
    {
        $this->repository = $repository;
        $this->publisher  = $publisher;
    }

    public function __invoke(string $keyword)
    {
        $site = Site::fromValues([]);

        $this->repository->saveSite($site);

        $this->publishSite($site, $keyword);
    }

    private function publishSite(Site $site, string $keyword)
    {
        $this->publisher->publish(
            [
                $this->createSiteDomainEvent($site, $keyword),
            ]
        );
    }

    private function createSiteDomainEvent(Site $site, string $keyword): DomainEvent
    {
        $currentDateTime = new \DateTime();

        return new SiteCreatedDomainEvent(
            (string) $site->id(),
            [
                'keyword'   => $keyword,
                'createdAt' => $currentDateTime->getTimestamp(),
            ]
        );
    }
}
