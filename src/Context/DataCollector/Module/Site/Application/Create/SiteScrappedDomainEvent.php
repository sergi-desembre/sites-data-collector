<?php

declare(strict_types = 1);

namespace DataCollector\Module\Site\Application\Create;

use DataCollector\Infrastructure\Bus\Event\DomainEvent;

final class SiteScrappedDomainEvent extends DomainEvent
{
    public function keyword(): string
    {
        return $this->data['keyword'];
    }

    public static function eventName(): string
    {
        return 'site_scrapped';
    }

    protected function rules(): array
    {
        return [
            'keyword' => ['string'],
        ];
    }

    protected function generatedByDataCollector(): bool
    {
        return true;
    }
}
