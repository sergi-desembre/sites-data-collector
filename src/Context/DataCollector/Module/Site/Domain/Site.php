<?php

declare(strict_types = 1);

namespace DataCollector\Module\Site\Domain;

use App\Context\Shared\Types\AggregateRoot;
use DataCollector\Infrastructure\Uuid\UuidGenerator;

final class Site extends AggregateRoot
{
    public $id;
    private $uuid;
    private $isActive;
    private $isDeleted;
    private $createdAt;

    public function __construct(
        string $uuid,
        \DateTimeImmutable $createdAt,
        bool $isActive = false,
        bool $isDeleted = false
    ) {
        $this->uuid      = $uuid;
        $this->isActive  = $isActive;
        $this->isDeleted = $isDeleted;
        $this->createdAt = $createdAt;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function uuid(): string
    {
        return $this->uuid;
    }

    public static function fromValues(array $values): Site
    {
        return new self(
            UuidGenerator::generate(),
            new \DateTimeImmutable()
        );
    }
}
