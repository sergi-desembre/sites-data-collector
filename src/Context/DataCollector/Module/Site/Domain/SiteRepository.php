<?php

declare(strict_types = 1);

namespace DataCollector\Module\Site\Domain;

interface SiteRepository
{
    public function saveSite(Site $site);
}
