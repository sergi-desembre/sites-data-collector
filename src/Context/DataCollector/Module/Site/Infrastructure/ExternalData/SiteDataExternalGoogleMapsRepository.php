<?php

declare(strict_types = 1);

namespace DataCollector\Module\Site\Infrastructure\ExternalData;

use DataCollector\Infrastructure\Location\GoogleMaps\GoogleMapsApiClient;
use DataCollector\Infrastructure\Location\GoogleMaps\GoogleMapsApiSearchFields;
use DataCollector\Infrastructure\Location\GoogleMaps\GoogleMapsApiSearchParameters;
use DataCollector\Infrastructure\Location\GoogleMaps\GoogleMapsApiSearchUrlAddress;
use DataCollector\Module\Site\Infrastructure\Presentation\DataTransformer\AskSiteFetcherGoogleMapsApiToGoogleMapsSite;
use DataCollector\Module\SiteData\Domain\GoogleMapsSite\GoogleMapsSite;
use DataCollector\Module\SiteData\Domain\GoogleMapsSite\GoogleMapsSiteCollection;
use DataCollector\Module\SiteData\Domain\SiteDataExtractorRepository;

final class SiteDataExternalGoogleMapsRepository extends GoogleMapsApiClient implements SiteDataExtractorRepository
{
    public function extractByKeyword(string $keyword): GoogleMapsSiteCollection
    {
        $response = $this->search(
            GoogleMapsApiSearchUrlAddress::buildAsSearch(),
            GoogleMapsApiSearchParameters::buildAsSearch($keyword)
        );

        $candidates = $response['candidates'];

        return new GoogleMapsSiteCollection(
            array_map(
                function (array $candidate) {
                    return $this->searchDetailByPlaceId($candidate['place_id']);
                },
                $candidates
            )
        );
    }

    public function searchDetailByPlaceId(string $placeId): GoogleMapsSite
    {
        return AskSiteFetcherGoogleMapsApiToGoogleMapsSite::transform(
            $this->search(
                GoogleMapsApiSearchUrlAddress::buildAsDetail(),
                GoogleMapsApiSearchParameters::buildAsDetail($placeId, GoogleMapsApiSearchFields::buildAsBasic())
            )
        );
    }
}
