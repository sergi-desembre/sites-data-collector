<?php

declare(strict_types = 1);

namespace DataCollector\Module\Site\Infrastructure\Persistence;

use DataCollector\Infrastructure\Orm\Doctrine\Repository;
use DataCollector\Module\Site\Domain\Site;
use DataCollector\Module\Site\Domain\SiteRepository;

final class SiteRepositoryPostgresSql extends Repository implements SiteRepository
{
    public function saveSite(Site $site)
    {
        $this->persist($site);
    }
}
