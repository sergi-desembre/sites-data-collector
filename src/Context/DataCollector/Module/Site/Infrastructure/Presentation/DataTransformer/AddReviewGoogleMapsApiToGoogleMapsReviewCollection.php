<?php

declare(strict_types = 1);

namespace DataCollector\Module\Site\Infrastructure\Presentation\DataTransformer;

use DataCollector\Module\Site\Domain\GoogleMapsSite\GoogleMapsSiteReview;
use DataCollector\Module\Site\Domain\GoogleMapsSite\GoogleMapsSiteReviewCollection;

final class AddReviewGoogleMapsApiToGoogleMapsReviewCollection
{
    private const FIELD_RESULT            = 'result';
    private const FIELD_REVIEWS           = 'reviews';
    private const FIELD_AUTHOR_NAME       = 'author_name';
    private const FIELD_AUTHOR_URL        = 'author_url';
    private const FIELD_LANGUAGE          = 'language';
    private const FIELD_PROFILE_PHOTO_URL = 'profile_photo_url';
    private const FIELD_RATING            = 'rating';
    private const FIELD_TEXT              = 'text';
    private const FIELD_TIME              = 'time';

    public static function transform(array $data): GoogleMapsSiteReviewCollection
    {
        $data = $data[self::FIELD_RESULT];

        return new GoogleMapsSiteReviewCollection(
            array_map(
                function (array $review) {
                    return self::createGoogleMapsSiteReview($review);
                },
                $data[self::FIELD_REVIEWS]
            )
        );
    }

    private static function createGoogleMapsSiteReview(array $review): GoogleMapsSiteReview
    {
        return GoogleMapsSiteReview::buildFromData(
            $review[self::FIELD_AUTHOR_NAME],
            $review[self::FIELD_AUTHOR_URL],
            $review[self::FIELD_PROFILE_PHOTO_URL],
            in_array(self::FIELD_LANGUAGE, $review) ? $review[self::FIELD_LANGUAGE] : null,
            $review[self::FIELD_RATING],
            $review[self::FIELD_TEXT],
            $review[self::FIELD_TIME]
        );
    }
}
