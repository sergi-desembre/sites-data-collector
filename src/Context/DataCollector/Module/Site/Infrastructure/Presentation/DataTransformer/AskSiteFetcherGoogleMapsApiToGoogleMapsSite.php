<?php

declare(strict_types=1);

namespace DataCollector\Module\Site\Infrastructure\Presentation\DataTransformer;

use App\Context\Shared\Domain\Location\Location;
use App\Context\Shared\Domain\Url\Url;
use DataCollector\Module\SiteData\Domain\GoogleMapsSite\GoogleMapsSite;
use DataCollector\Module\SiteData\Domain\GoogleMapsSite\GoogleMapsSitePhoto;
use DataCollector\Module\SiteData\Domain\GoogleMapsSite\GoogleMapsSitePhotoCollection;
use Ramsey\Uuid\Uuid;

final class AskSiteFetcherGoogleMapsApiToGoogleMapsSite
{
    private const FIELD_RESULT                     = 'result';
    private const FIELD_PLACE_ID                   = 'place_id';
    private const FIELD_NAME                       = 'name';
    private const FIELD_PHOTOS                     = 'photos';
    private const FIELD_PHOTO_REFERENCE            = 'photo_reference';
    private const FIELD_WIDTH                      = 'width';
    private const FIELD_HEIGHT                     = 'height';
    private const FIELD_LATITUDE                   = 'lat';
    private const FIELD_LONGITUDE                  = 'lng';
    private const FIELD_URL                        = 'url';
    private const FIELD_GEOMETRY                   = 'geometry';
    private const FIELD_LOCATION                   = 'location';
    private const FIELD_FORMATTED_ADDRESS          = 'formatted_address';
    private const FIELD_INTERNATIONAL_PHONE_NUMBER = 'international_phone_number';

    public static function transform(array $data): GoogleMapsSite
    {
        $site = $data[self::FIELD_RESULT];

        return new GoogleMapsSite(
            Uuid::uuid4(),
            $site[self::FIELD_PLACE_ID],
            $site[self::FIELD_NAME],
            in_array(self::FIELD_INTERNATIONAL_PHONE_NUMBER, $site) ?
                $site[self::FIELD_INTERNATIONAL_PHONE_NUMBER] : null,
            $site[self::FIELD_FORMATTED_ADDRESS],
            new Url($site[self::FIELD_URL]),
            new Location(
                $site[self::FIELD_GEOMETRY][self::FIELD_LOCATION][self::FIELD_LATITUDE],
                $site[self::FIELD_GEOMETRY][self::FIELD_LOCATION][self::FIELD_LONGITUDE]
            ),
            self::createGoogleMapsSitePhotoCollection($site)
        );
    }

    private static function createGoogleMapsSitePhotoCollection(array $googleMapsData): GoogleMapsSitePhotoCollection
    {
        $photos = isset($googleMapsData[self::FIELD_PHOTOS]) ? $googleMapsData[self::FIELD_PHOTOS] : [];

        return new GoogleMapsSitePhotoCollection(
            array_map(
                function (array $review) {
                    return new GoogleMapsSitePhoto(
                        $review[self::FIELD_PHOTO_REFERENCE],
                        $review[self::FIELD_WIDTH],
                        $review[self::FIELD_HEIGHT]
                    );
                },
                $photos
            )
        );
    }
}
