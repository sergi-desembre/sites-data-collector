<?php

declare(strict_types = 1);

namespace DataCollector\Module\SiteData\Application\Extract;

use App\Context\Shared\Domain\Site\SiteCreatedDomainEvent;
use DataCollector\Module\Site\Domain\SiteKeyword;
use function Lambdish\Phunctional\apply;

final class ExtractSiteDataOnSiteCreated
{
    private $extractor;

    public function __construct(SiteDataExtractor $extractor)
    {
        $this->extractor = $extractor;
    }

    public function __invoke(SiteCreatedDomainEvent $event)
    {
        $siteKeyword = new SiteKeyword($event->keyword());

        apply($this->extractor, [$siteKeyword->keyword()]);
    }
}
