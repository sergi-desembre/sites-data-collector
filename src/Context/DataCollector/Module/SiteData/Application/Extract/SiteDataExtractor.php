<?php

declare(strict_types = 1);

namespace DataCollector\Module\SiteData\Application\Extract;

use DataCollector\Module\SiteData\Domain\SiteDataExtractorRepository;

final class SiteDataExtractor
{
    private $extractorRepositories;

    public function __construct(SiteDataExtractorRepository ...$extractorRepositories)
    {
        $this->extractorRepositories = $extractorRepositories;
    }

    public function __invoke(string $keyword)
    {
        $dataExtracted = array_map(function(SiteDataExtractorRepository $extractorRepository) use ($keyword) {
            return $extractorRepository->extractByKeyword($keyword);
        }, $this->extractorRepositories);

        print_r($dataExtracted); die();
    }
}
