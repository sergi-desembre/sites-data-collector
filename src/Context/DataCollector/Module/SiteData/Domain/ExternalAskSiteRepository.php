<?php

namespace DataCollector\Module\SiteData\Domain;

use DataCollector\Module\SiteData\Domain\GoogleMapsSite\GoogleMapsSiteCollection;
use DataCollector\Module\SiteData\Domain\GoogleMapsSite\GoogleMapsSiteReviewCollection;

interface ExternalAskSiteRepository
{
    public function searchByKeyword(string $keyword): GoogleMapsSiteCollection;

    public function searchReviewsByPlaceId(string $placeId): GoogleMapsSiteReviewCollection;
}
