<?php

declare(strict_types=1);

namespace DataCollector\Module\SiteData\Domain\GoogleMapsSite;

use App\Context\Shared\Domain\Location\Location;
use App\Context\Shared\Domain\Url\Url;
use Ramsey\Uuid\UuidInterface;

final class GoogleMapsSite
{
    private $uuid;
    private $googleId;
    private $name;
    private $phone;
    private $formattedAddress;
    private $url;
    private $location;
    private $photos;

    public function __construct(
        UuidInterface $uuid,
        string $googleId,
        string $name,
        ?string $phone,
        string $formattedAddress,
        Url $url,
        Location $location,
        GoogleMapsSitePhotoCollection $googleMapsSitePhotoCollection
    ) {
        $this->uuid             = $uuid;
        $this->googleId         = $googleId;
        $this->name             = $name;
        $this->phone            = $phone;
        $this->formattedAddress = $formattedAddress;
        $this->url              = $url;
        $this->location         = $location;
        $this->photos           = $googleMapsSitePhotoCollection;
    }

    public function formattedUuid(): string
    {
        return $this->uuid->toString();
    }

    public function googleId(): string
    {
        return $this->googleId;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function phone(): ?string
    {
        return $this->phone;
    }

    public function address(): string
    {
        return $this->formattedAddress;
    }

    public function url(): Url
    {
        return $this->url;
    }

    public function location(): Location
    {
        return $this->location;
    }

    public function photos(): GoogleMapsSitePhotoCollection
    {
        return $this->photos;
    }

    public function hash(): string
    {
        return md5(
            json_encode(
                [
                    $this->name,
                    $this->phone,
                    $this->formattedAddress,
                    $this->url->completeAddress(),
                    $this->location()->latitude(),
                    $this->location()->longitude(),
                ]
            )
        );
    }
}
