<?php

declare(strict_types=1);

namespace DataCollector\Module\SiteData\Domain\GoogleMapsSite;

final class GoogleMapsSiteAuthor
{
    private $name;
    private $url;
    private $image;

    public function __construct(string $name, string $url, string $image)
    {
        $this->name  = $name;
        $this->url   = $url;
        $this->image = $image;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function url(): string
    {
        return $this->url;
    }

    public function image(): string
    {
        return $this->image;
    }
}
