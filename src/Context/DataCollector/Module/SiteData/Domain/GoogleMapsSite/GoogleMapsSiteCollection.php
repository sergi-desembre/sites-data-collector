<?php

declare(strict_types=1);

namespace DataCollector\Module\SiteData\Domain\GoogleMapsSite;

use App\Context\Shared\Types\Collection;

final class GoogleMapsSiteCollection extends Collection
{
    protected function type(): string
    {
        return GoogleMapsSite::class;
    }
}
