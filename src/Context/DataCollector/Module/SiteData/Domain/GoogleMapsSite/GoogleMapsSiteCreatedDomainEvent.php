<?php

declare(strict_types = 1);

namespace DataCollector\Module\SiteData\Domain\GoogleMapsSite;

use DataCollector\Infrastructure\Bus\Event\DomainEvent;

final class GoogleMapsSiteCreatedDomainEvent extends DomainEvent
{
    public static function eventName(): string
    {
        return 'google_maps_site_created';
    }

    protected function rules(): array
    {
        return [
            'externalId' => ['string'],
            'name'        => ['string'],
            'url'         => ['string'],
            'createdAt'   => ['int'],
        ];
    }

    protected function generatedByDataCollector(): bool
    {
        return true;
    }
}
