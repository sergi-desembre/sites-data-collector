<?php

declare(strict_types=1);

namespace DataCollector\Module\SiteData\Domain\GoogleMapsSite;

final class GoogleMapsSitePhoto
{
    private $reference;
    private $height;
    private $width;

    public function __construct(string $reference, int $height, int $width)
    {
        $this->reference = $reference;
        $this->height    = $height;
        $this->width     = $width;
    }
}
