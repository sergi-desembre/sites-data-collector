<?php

declare(strict_types=1);

namespace DataCollector\Module\SiteData\Domain\GoogleMapsSite;

use App\Context\Shared\Domain\Rating\Rating;

final class GoogleMapsSiteReview
{
    private $author;
    private $language;
    private $rating;
    private $text;
    private $time;

    private function __construct(GoogleMapsSiteAuthor $author, ?string $language, Rating $rating, string $text, int $time)
    {
        $this->author   = $author;
        $this->language = $language;
        $this->rating   = $rating;
        $this->text     = $text;
        $this->time     = $time;
    }

    public function author(): GoogleMapsSiteAuthor
    {
        return $this->author;
    }

    public function language(): ?string
    {
        return $this->language;
    }

    public function rating(): Rating
    {
        return $this->rating;
    }

    public function text(): string
    {
        return $this->text;
    }

    public function timeInternationalFormat()
    {
        $dateTime = date_create()->setTimestamp($this->time);

        return $dateTime->format(DATE_ISO8601);
    }

    public static function buildFromData(
        string $authorName,
        string $authorUrl,
        string $authorImage,
        ?string $language,
        int $rating,
        string $text,
        int $time
    ): GoogleMapsSiteReview
    {
        $author = new GoogleMapsSiteAuthor($authorName, $authorUrl, $authorImage);
        $rating = new Rating($rating);

        return new self($author, $language, $rating, $text, $time);
    }
}
