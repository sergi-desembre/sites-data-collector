<?php

declare(strict_types=1);

namespace DataCollector\Module\SiteData\Domain\GoogleMapsSite;

use App\Context\Shared\Types\Collection;

final class GoogleMapsSiteReviewCollection extends Collection
{
    protected function type(): string
    {
        return GoogleMapsSiteReview::class;
    }
}
