<?php

declare(strict_types = 1);

namespace DataCollector\Module\SiteData\Domain\GoogleMapsSite;

use DataCollector\Infrastructure\Bus\Event\DomainEvent;

final class StoreDatabaseOnGoogleMapsSiteCreated
{
    public function __invoke(DomainEvent $event)
    {
        print_r($event->data());
//        echo 'Stored to database!!!'; die();
    }
}
