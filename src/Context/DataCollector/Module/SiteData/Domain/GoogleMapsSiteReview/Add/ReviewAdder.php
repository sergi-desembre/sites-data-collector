<?php

declare(strict_types = 1);

namespace DataCollector\Module\SiteData\Domain\GoogleMapsSiteReview\Add;

use DataCollector\Module\SiteData\Domain\ExternalAskSiteRepository;
use DataCollector\Module\SiteData\Domain\GoogleMapsSite\GoogleMapsSiteReview;

final class ReviewAdder
{
    private $repository;

    public function __construct(ExternalAskSiteRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(string $placeId)
    {
        $reviews = $this->repository->searchReviewsByPlaceId($placeId);

        echo 'Google place id ' . $placeId . ' total reviews ' . $reviews->count() . PHP_EOL;
        array_map(function(GoogleMapsSiteReview $review) {
            echo $review->author()->name() .
                 ' at ' .
                 $review->timeInternationalFormat() .
                 ' review:' .
                 PHP_EOL .
                 $review->text() .
                 PHP_EOL .
                 '------------------------------------------------------------------------------' .
                 PHP_EOL;
        }, $reviews->items());
    }
}
