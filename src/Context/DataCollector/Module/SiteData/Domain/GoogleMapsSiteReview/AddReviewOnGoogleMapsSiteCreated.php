<?php

declare(strict_types = 1);

namespace DataCollector\Module\SiteData\Domain\GoogleMapsSiteReview;

use DataCollector\Infrastructure\Bus\Event\DomainEvent;
use DataCollector\Module\SiteData\Domain\GoogleMapsSiteReview\Add\ReviewAdder;
use function Lambdish\Phunctional\apply;

final class AddReviewOnGoogleMapsSiteCreated
{
    private $adder;

    public function __construct(ReviewAdder $adder)
    {
        $this->adder = $adder;
    }

    public function __invoke(DomainEvent $event)
    {
        $data = $event->data();

        apply($this->adder, [$data['externalId']]);
    }
}
