<?php

declare(strict_types = 1);

namespace DataCollector\Module\SiteData\Domain;

use DataCollector\Module\SiteData\Domain\GoogleMapsSite\GoogleMapsSiteCollection;

interface SiteDataExtractorRepository
{
    public function extractByKeyword(string $keyword): GoogleMapsSiteCollection;
}
