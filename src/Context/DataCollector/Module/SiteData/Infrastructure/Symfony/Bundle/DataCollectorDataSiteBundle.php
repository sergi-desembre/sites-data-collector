<?php

declare(strict_types = 1);

namespace DataCollector\Module\SiteData\Infrastructure\Symfony\Bundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class DataCollectorDataSiteBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }
}
