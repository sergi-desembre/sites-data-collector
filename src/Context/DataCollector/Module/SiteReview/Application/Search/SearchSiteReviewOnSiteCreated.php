<?php

declare(strict_types = 1);

namespace DataCollector\Module\SiteReview\Application\Search;

use function Lambdish\Phunctional\apply;

final class SearchSiteReviewOnSiteCreated
{
    private $searcher;

    public function __construct(SiteReviewSearcher $searcher)
    {
        $this->searcher = $searcher;
    }

    public function __invoke(SiteCreatedDomainEvent $event)
    {
        apply($this->searcher, [$event->aggregateId()]);
    }
}
