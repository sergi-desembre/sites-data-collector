<?php

declare(strict_types = 1);

namespace DataCollector\Module\SiteReview\Application\Search;

use DataCollector\Module\SiteReview\Domain\SiteReviewApiRepository;

final class SiteReviewSearcher
{
    private $apiRepository;

    public function __construct(SiteReviewApiRepository $apiRepository)
    {
        $this->apiRepository = $apiRepository;
    }

    public function __invoke($siteId)
    {
        echo '-------------------------------------' . PHP_EOL;
        echo $siteId; die();
    }
}
