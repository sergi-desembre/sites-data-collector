<?php

declare(strict_types = 1);

namespace DataCollector\Module\SiteReview\Domain;

interface SiteReviewApiRepository
{
    public function search();
}
