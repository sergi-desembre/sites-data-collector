<?php

declare(strict_types = 1);

namespace App\Context\Shared\Domain\Monitoring;

interface Monitor
{
    public function onePoint(array $tags);
}
