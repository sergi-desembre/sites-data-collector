<?php

declare(strict_types=1);

namespace App\Context\Shared\Domain\Rating;

use DomainException;

final class RateValueGreaterThanMaxValue extends DomainException
{
    public function __construct(int $maxValue, int $value)
    {
        parent::__construct(sprintf('The rate value <%s> is greater than max value <%s>', $value, $maxValue));
    }

    public static function errorCode(): string
    {
        return 'rate_vale_grater_max_value';
    }
}
