<?php

declare(strict_types=1);

namespace App\Context\Shared\Domain\Rating;

final class Rating
{
    public const DEFAULT_VALUE     = 0;
    public const DEFAULT_MAX_VALUE = 5;

    private $value;
    private $maxValue;

    public function __construct(int $value = self::DEFAULT_VALUE, int $maxValue = self::DEFAULT_MAX_VALUE)
    {
        $this->guard($maxValue, $value);

        $this->value    = $value;
        $this->maxValue = $maxValue;
    }

    public function value(): int
    {
        return $this->value;
    }

    public function maxValue(): int
    {
        return $this->maxValue;
    }

    private function guard(int $maxValue, int $value)
    {
        if ($value > $maxValue) {
            throw new RateValueGreaterThanMaxValue($maxValue, $value);
        }
    }
}
