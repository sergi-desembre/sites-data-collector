<?php

declare(strict_types = 1);

namespace App\Context\Shared\Domain\Site;

use DataCollector\Infrastructure\Bus\Event\DomainEvent;

final class SiteCreatedDomainEvent extends DomainEvent
{
    public function keyword(): string
    {
        return $this->data['keyword'];
    }

    public static function eventName(): string
    {
        return 'site_created';
    }

    protected function rules(): array
    {
        return [
            'keyword'   => ['string'],
            'createdAt' => ['int'],
        ];
    }

    protected function generatedByDataCollector(): bool
    {
        return true;
    }
}
