<?php

declare(strict_types=1);

namespace App\Context\Shared\Domain\Url;

use DomainException;

final class InvalidUrlAddressException extends DomainException
{
    public function __construct(string $url)
    {
        parent::__construct(sprintf('The URL address <%s> is not valid', $url));
    }

    public static function errorCode(): string
    {
        return 'invalid_url_address';
    }
}
