<?php

declare(strict_types=1);

namespace App\Context\Shared\Domain\Url;

final class Url
{
    private $completeAddress;
    private $scheme;
    private $host;
    private $port;
    private $path;
    private $query;

    public function __construct(string $completeAddress)
    {
        $this->guard($completeAddress);

        $this->completeAddress = $completeAddress;
        $this->scheme          = parse_url($completeAddress, PHP_URL_SCHEME);
        $this->host            = parse_url($completeAddress, PHP_URL_HOST);
        $this->port            = parse_url($completeAddress, PHP_URL_PORT);
        $this->path            = parse_url($completeAddress, PHP_URL_PATH);
        $this->query           = parse_url($completeAddress, PHP_URL_QUERY);
    }

    private function guard(string $completeAddress)
    {
        if (!filter_var($completeAddress, FILTER_VALIDATE_URL)) {
            throw new InvalidUrlAddressException($completeAddress);
        }
    }

    public function completeAddress(): string
    {
        return $this->completeAddress;
    }
}
