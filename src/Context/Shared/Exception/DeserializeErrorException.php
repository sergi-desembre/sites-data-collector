<?php

namespace App\Context\Shared\Exception;

use RuntimeException;

final class DeserializeErrorException extends RuntimeException
{
    private $lastErrorMessage;

    public function __construct($lastErrorMessage)
    {
        parent::__construct(sprintf('JSON decoding error: %s', $lastErrorMessage));

        $this->lastErrorMessage = $lastErrorMessage;
    }

    public function lastErrorMessage()
    {
        return $this->lastErrorMessage;
    }
}
