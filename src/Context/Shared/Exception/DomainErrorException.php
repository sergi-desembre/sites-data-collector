<?php

namespace App\Context\Shared\Exception;

use DomainException;

abstract class DomainErrorException extends DomainException
{
    public function __construct()
    {
        parent::__construct($this->errorMessage());
    }

    abstract public function errorCode(): string;

    abstract protected function errorMessage(): string;
}
