<?php

declare(strict_types = 1);

namespace App\Context\Shared\Types;

use DataCollector\Infrastructure\Bus\Event\DomainEvent;

abstract class AggregateRoot
{
    private $domainEvents = [];

    final public function pullDomainEvents()
    {
        $domainEvents       = $this->domainEvents;
        $this->domainEvents = [];

        return $domainEvents;
    }

    final protected function raise(DomainEvent $domainEvent)
    {
        $this->domainEvents[] = $domainEvent;
    }
}
