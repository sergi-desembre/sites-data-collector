<?php

declare(strict_types = 1);

namespace App\Context\Shared\Types;

use function Lambdish\Phunctional\reduce;

abstract class AggregateRootCollection extends Collection
{
    public function pullDomainEvents(): array
    {
        return reduce($this->pullItemDomainEvents(), $this, []);
    }

    public function items(): array
    {
        return $this->items();
    }

    private function pullItemDomainEvents()
    {
        return function (array $accumulatedEvents, AggregateRoot $aggregateRoot) {
            return array_merge($accumulatedEvents, $aggregateRoot->pullDomainEvents());
        };
    }
}
