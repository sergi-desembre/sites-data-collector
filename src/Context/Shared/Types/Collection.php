<?php

declare(strict_types = 1);

namespace App\Context\Shared\Types;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use function Lambdish\Phunctional\each;

abstract class Collection implements Countable, IteratorAggregate
{
    protected $items;

    public function __construct(array $items)
    {
        $class = $this->type();

        foreach ($items as $item) {
            if (!$item instanceof $class) {
                throw new \InvalidArgumentException(
                    sprintf('The object <%s> is not an instance of <%s>', $class, get_class($item))
                );
            }
        }

        $this->items = $items;
    }

    abstract protected function type(): string;

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items());
    }

    public function count(): int
    {
        return count($this->items());
    }

    protected function each(callable $fn)
    {
        each($fn, $this->items());
    }

    public function items(): array
    {
        return $this->items;
    }
}
