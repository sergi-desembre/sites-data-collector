<?php

declare(strict_types = 1);

namespace App\Context\Shared\Types;

class Identifier
{
    private $identifier;

    public function __construct($identifier)
    {
        $this->guard($identifier);

        $this->identifier = $identifier;
    }

    public function identifier()
    {
        return $this->identifier;
    }

    private function guard($identifier)
    {
        if (!is_string($identifier) && !is_int($identifier)) {
            throw new \InvalidArgumentException(
                sprintf(
                    '<%s> does not allow the identifier <%s>.',
                    static::class,
                    is_scalar($identifier) ? $identifier : gettype($identifier)
                )
            );
        }
    }
}
