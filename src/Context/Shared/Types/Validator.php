<?php

declare(strict_types = 1);

namespace App\Context\Shared\Types;

use RuntimeException;
use function Lambdish\Phunctional\get;

final class Validator
{
    public static function isValid($value, $type)
    {
        $validator = self::validatorFor($type);

        return $validator($value);
    }

    private static function validatorFor(string $type): callable
    {
        $validators = [
            'string'  => self::stringValidator(),
            'int'     => self::intValidator(),
            'float'   => self::floatValidator(),
            'numeric' => self::numericValidator(),
            'array'   => self::arrayValidator(),
            'bool'    => self::boolValidator(),
            '*'       => self::allValidator(),
        ];

        $validator = get($type, $validators);

        if (null === $validator) {
            throw new RuntimeException(sprintf('There is not validator defined for the <%s> type', $type));
        }

        return $validator;
    }

    private static function stringValidator()
    {
        return function ($value) {
            return is_string($value);
        };
    }

    private static function intValidator()
    {
        return function ($value) {
            return is_int($value);
        };
    }

    private static function arrayValidator()
    {
        return function ($value) {
            return is_array($value);
        };
    }

    private static function floatValidator()
    {
        return function ($value) {
            return is_float($value);
        };
    }

    private static function numericValidator()
    {
        return function ($value) {
            return is_numeric($value) || '' === $value;
        };
    }

    private static function boolValidator()
    {
        return function ($value) {
            return is_bool($value);
        };
    }

    private static function allValidator()
    {
        return function ($unused) {
            return true;
        };
    }
}
