<?php

declare(strict_types = 1);

namespace App\Context\Shared\Utils;

use App\Context\Shared\Exception\DeserializeErrorException;

final class JsonDeserialize
{
    public static function run(string $valueToDeserialize)
    {
        $deserializedValue = json_decode($valueToDeserialize, true);

        if (json_last_error() == JSON_ERROR_NONE) {
            return $deserializedValue;
        }

        throw new DeserializeErrorException(json_last_error_msg());
    }
}
